const getAllOrder = (req, res, next) => {
    console.log("Get All Order");
    next()
};
const getOrder = (req, res, next) => {
    console.log("Get a Order");
    next();
};
const postOrder = (req, res, next) => {
    console.log("Post a Order");
    next();
}
const putOrder = (req, res, next) => {
    console.log("Put a Order");
    next();
}
const deleteOrder = (req, res, next) => {
    console.log("Delete a Order");
    next();
}

module.exports = {
    getAllOrder,
    getOrder,
    postOrder,
    putOrder,
    deleteOrder
}