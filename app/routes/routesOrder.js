const express = require("express")

const  {
    getAllOrder,
    getOrder,
    postOrder,
    putOrder,
    deleteOrder
} = require ("../middleware/middlewareOrder");

const router = express.Router();

router.get("/" ,getAllOrder ,(req, res) => {
    res.json({
        message: "Get all Order"
    })
});

router.get("/get/:vocherId" ,getOrder ,(req, res) => {
    let vocherId = req.params.vocherId
    res.json({
        message: `Order ID: ${vocherId}`
    })
});
router.post("/post" ,postOrder ,(req, res) => {
    res.json({
        message: "POST a Order"
    })
});
router.put("/put" ,putOrder ,(req, res) => {
    res.json({
        message: "PUT a Order"
    })
});
router.delete("/delete" ,deleteOrder ,(req, res) => {
    res.json({
        message: "DELETE a Order"
    })
});

module.exports = {router};